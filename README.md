# Foodsby's Data Science Challenge (DSC)
##### Proprietary and Confidential
------------

You are working for Foodsby's data science team. Some of our teammembers asked us for our help, Can you help them?

Our amazing marketing team wants to test a new marketing campain. The team wants to notify our users when their favorite cuisine is delivering in the upcoming week. Each user has a profile where they can set their favorite cuisine, but only 76.7% of our users have set their favorite cuisine. We want to make sure we notify all users. Can you help us fill in the missing favorite cuisines?

The file **`user_profile.csv`** contains a list of each user's customer_id and their favorite_cuisine.

You are given the file, **`orders.csv`**, which contains a list of orders by a Foodsby user from a resturant with the menu_item they ordered. Additionally, the **`restaurants.csv`** provides you with a list of our restaurants, and lastly, the **`menu.csv`** file is a list of each restaurant's menu items.

**`orders.csv`**, which contains a list of Foodsby orders over the past two years.
The columns of this dataset are as follows:

- **`order_id`** unique id representing an order
- **`order_date`** date of order
- **`delivery_time`** estimated time the food will be droped off at the user's address
- **`restaurant_id`** unique id representing a restaurant, joins to `resaurant_id` in **`restaurants.csv`**
- **`menu_item_id`** unique id representing a restaurant's menu item. To join to **`menu.csv`** join by `resaurant_id` ane `menu_id` 
- **`customer_id`** unique id representing a Foodsby user

------------

Tasks:

- Train a machine learning model that predicts a user's favorite cuisine given the data in the three tables (csv files)
- For the 24.3% users that have a missing favorite cuisine, create a new csv named **`users_predicted_favorite_cuisine.csv`**. Please add two columns to the csv, customer_id and predicted_favorite_cuisine.

Please use Python - feel free to use any libraries you see fit. Note that in reviewing your challenge, we'll be assessing your scientific approach, code quality and clarity of thought in working through the problem.

------------

## Submission instructions

**Please follow all listed steps to ensure a prompt review of your submission by our data hiring team:**

1. Provide your Foodsby point of contact with your email address (create a new account if you do not already have one). Your point of contact will add you to the data challenge repository `foodsby-dsc`. Once you've been added to the repo and accept the invitation, you should see challenge instruction in the readme.md of the repo.
2. Fork the `foodsby-dsc` repository. How to fork on bitbucket - Click the plus `+`. Next click `Fork this repo`.
3. In your forked `foodsby-dsc` repo, click on `Repository Settings` and then click on `Users and group access`. Under `Users`, please type in `nick.stang@foodsby.com` and choose the `Admin` role permission. When done, click `Add`. *This will give us permissions to review your challenge submission when complete.*
4. In the forked `foodsby-dsc` project, create a new branch `lastname-firstname`. Work on the assignment and commit your changes to the `lastname-firstname` branch.
5. When complete with the assignment, after having committed all your changes, create a new `Pull Request` from `lastname-firstname` to `master` in your repo. Add `nick.stang@foodsby.com` as an assignee. *This will trigger an email notification to review your submission.* Please do not create a pull request back to the original repo.
6. Email Meghan.Weiland@foodsby.com to let her know you completed the challenge
	
* If any questions come up, please send an email to `nick.stang@foodsby.com`.